# 1.0.0 (2020-04-02)


### Features

* Add a custom `vmware_guest.py` ([8c714a4](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/8c714a4))
* Add the ability to use ks files with vmware ([518b88c](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/518b88c))
* Merge in changes from b network ([fa96cee](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/fa96cee))
* Update the VMWare provisioner ([e40f28e](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/e40f28e))
* Update vmware provisioner ([f232c39](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/f232c39))
* Updates to VMWare Provisioner ([12a3ff9](https://gitlab.com/dreamer-labs/maniac/ansible_role_provisioning/commit/12a3ff9))
