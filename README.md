ansible_role_provisioning
=========

This role can be used to provision instances in either OpenStack or VMWare VCenter clusters.

Requirements
------------

To be completed

Role Variables
--------------

To be completed

Dependencies
------------

None

Example Playbook
----------------

 ```yaml
 ---
 - name: Provision instances
   hosts: localhost
   gather_facts: false
   roles:
     - role: ansible-role-provision

```

License
-------

MIT

Author Information
------------------

Global InfoTek, Inc
